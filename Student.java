public class Student{
	private String schoolName;
	private int age;
	private String favColor;
	private int amountLearnt;
	
	public Student(int age, String favColor){
		this.schoolName= "Bird";
		this.age=age;
		this.favColor=favColor;		
		this.amountLearnt=0;
	}
	public Student(String studentSchoolName, int studentAge, String studentfavColor){
		schoolName= studentSchoolName;
		age= studentAge;
		favColor= studentfavColor;
	}
	
	public String greetFromSchool(){
		return "hello! I come from "+schoolName+".";
	}
	public String ColorOfCar(){
		return favColor;
	}
	public void study(int amountStudied){
		amountLearnt+= amountStudied;
	}
	public String getSchoolName(){
		return schoolName;
	}
	public int getAge(){
		return age;
	}
	public String getFavColor(){
		return favColor;
	}
	public int getAmountLearnt(){
		return amountLearnt;
	}
	
	
	public void setSchoolName(String schoolName){
		this.schoolName= schoolName;
	}
	
}