import java.util.Scanner;
public class Application{
	public static void main(String[] args){
		
		Student student1= new Student("Lucille", 30, "yellow");
		Student student2= new Student("Dawson", 16, "purple");
		
		Student[] section3= new Student[3];
		
		section3[0]= student1;
		section3[1]=student2;
		
		section3[2]= new Student("Mars", 99, "light green");
		
		System.out.println(section3[2].getAge());
		//bottom line doesnt work because i deleted setAge()
		//section3[2].setAge(66);
		System.out.println(section3[2].getAge());

		Scanner sc= new Scanner(System.in);
		System.out.print("amount to be studied: ");
		section3[2].study(Integer.parseInt(sc.nextLine()));
		
		System.out.println(section3[0].getAmountLearnt()); 
		System.out.println(section3[1].getAmountLearnt()); 
		System.out.println(section3[2].getAmountLearnt()); 
		
		Student student4= new Student(16, "yellow");
		student4.setSchoolName("Bread And Butter");
		System.out.println("\n\n"+ student4.getSchoolName());
		System.out.println(student4.getAge());
		System.out.println(student4.getFavColor());

	}
}